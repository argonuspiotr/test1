require 'csv'


class Student
	@@idd = 0
	attr_accessor :name, :surname, :specialization, :grades
	def initialize(name,surname,grades,specialization)
		@name = name.capitalize
		@surname = surname.capitalize
		@specialization = specialization
		@grades = grades
		@@idd += 1
		#raise "To many users" if @@idd > 10
	end

	def self.number
		@@idd
	end
#wszyscy studenci
	def self.all
		ObjectSpace.each_object(self).to_a
	end

	def srednia
		@grades.map{|a| a.to_i}.inject(:+) / @grades.size.to_f
	end

	def best_student
		Student.all.select{|s| s.srednia > 5}
	end

end #Student
#dla kazdego z users
CSV.foreach("users.csv") do |row|
	#puts row
	#puts "row inspect"
	#puts row.inspect
	#puts row[3].split(',').inspect
	#break
	Student.new(row[0],row[1],row[3].split(","),row[2])
end

puts Student.number
#puts Student.all.last.inspect
a = Student.all.last
puts a.srednia
puts a.best_student
puts a.inspect
#class Subject
	#attr_accessor :name
	#def initialize(name)
	#	@name = name
	#end
#
#class Grade
	#attr_accessor :created_at, :value, :subject
	#def initialize(created_at,value,subject)
		#@created_at = created_at
		#@value = value
		#@subject = subject
	#end
#end

#class AkademiaKoduStudent < Student
	#attr_accessor :name, :surname, :specialization, :grades
	#def initialize(name,surname,grades,specialization="ror")
		#super
	#end

#end


