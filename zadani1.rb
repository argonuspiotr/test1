class Student
	@@idd = 0
	attr_accessor :name, :surname, :specialization, :grades
	def initialize(name,surname,grades,specialization)
		@name = name.capitalize
		@surname = surname.capitalize
		@specialization = specialization
		@grades = grades
		@@idd += 1
		raise "To many users" if @@idd > 10
	end

	def self.number
		@@idd
	end

end

class Subject
	attr_accessor :name
	def initialize(name)
		@name = name
	end
end

class Grade
	attr_accessor :created_at, :value, :subject
	def initialize(created_at,value,subject)
		@created_at = created_at
		@value = value
		@subject = subject
	end
end

class AkademiaKoduStudent < Student
	attr_accessor :name, :surname, :specialization, :grades
	def initialize(name,surname,grades,specialization="ror")
		super
	end

end

student1=AkademiaKoduStudent.new("Piotr","Rybarczyk",3)
student1=AkademiaKoduStudent.new("Piotr","Rybarczyk",3)
student1=AkademiaKoduStudent.new("Piotr","Rybarczyk",3)
student1=AkademiaKoduStudent.new("Piotr","Rybarczyk",3)
student1=AkademiaKoduStudent.new("Piotr","Rybarczyk",3)
student1=AkademiaKoduStudent.new("Piotr","Rybarczyk",3)
student1=AkademiaKoduStudent.new("Piotr","Rybarczyk",3)
student1=AkademiaKoduStudent.new("Piotr","Rybarczyk",3)
student1=AkademiaKoduStudent.new("Piotr","Rybarczyk",3)
student1=AkademiaKoduStudent.new("Piotr","Rybarczyk",3)
puts Student.number
puts Student.number
puts student1.inspect