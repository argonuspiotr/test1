class Array
	def our_each
		i = 0
		while( i < self.size ) do
			yield( self[i] )
			i += 1
		end
	end
end

my_array = ["a", "b", "c"]
my_array.our_each { |letter| puts letter }
