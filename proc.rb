#obiekt przechowujacy funkcje


[1,2,3].map[|a| a*2]
b = Proc.new{ |b| b * 2}
[1,2,3,4].map(&b)

lam = lambda{|a| a * 2}
[1,2,3,4].map(&lam)

stabby_lam = ->(a) {a * 2}
[1,2,3,4].map(&stabby_lam)