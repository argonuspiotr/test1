def test_method
	yield
end

puts test_method { "robert" }

def calculate(a,b)
	puts yield(a,b)
end

calculate(5,6) {|a,b| a*b}

def twice_do(&proc)
	2.times { proc.call()} if proc
end
twice_do { puts "ok" } 

def thrice_do
	3.times { yield } if block_given?
end

thrice_do { puts "oko" }


